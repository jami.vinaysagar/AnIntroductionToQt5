#ifndef INIFILEFORMAT_H
#define INIFILEFORMAT_H

#include "addressbookfile.h"

#include <QObject>

/**
 * @brief The INI File format
 *
 * This class implement support for the INI file format. It derives from QObject and
 * also implements the AddressBookFile interface
 */
class INIFileFormat : public QObject, public AddressBookFile
{
    Q_OBJECT

    // Explicitly tell Qt that we implement this interface, allows using qobject_cast
    Q_INTERFACES( AddressBookFile )
public:
    explicit INIFileFormat(QObject *parent = 0);

    // AddressBookFile interface
    bool saveAddressBook(const AddressBook *addressBook, const QString &fileName) const override;
    bool loadAddressBook(AddressBook *addressBook, const QString &fileName) const override;
};

#endif // INIFILEFORMAT_H
