#ifndef JSONFILEFORMAT_H
#define JSONFILEFORMAT_H

#include "addressbookfile.h"

#include <QObject>

/**
 * @brief The JSONFileFormat class
 *
 * Implements reading and writing JSON files. It implements QObject and
 * the AddressBookFile interface.
 */
class JSONFileFormat : public QObject, public AddressBookFile
{
  Q_OBJECT
  Q_INTERFACES( AddressBookFile )
public:
  explicit JSONFileFormat(QObject *parent = 0);
  
  // AddressBookFile interface
  bool saveAddressBook(const AddressBook *addressBook, const QString &fileName ) const override;
  bool loadAddressBook(AddressBook *addressBook, const QString &fileName ) const override;
};

#endif // JSONFILEFORMAT_H
