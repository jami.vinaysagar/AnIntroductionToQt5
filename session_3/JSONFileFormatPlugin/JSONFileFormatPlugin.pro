QT       -= gui

TARGET = JSONFileFormatPlugin
TEMPLATE = lib
CONFIG += plugin c++11

SOURCES += jsonfileformatplugin.cpp \
    jsonfileformat.cpp

HEADERS += jsonfileformatplugin.h \
    jsonfileformat.h

# Put the resulting library in the AddressBookPlugins directory:
DESTDIR = ../bin/AddressBookPlugins

# Set up library search paths and link against AddressBookLibrary:
LIBS += -L../bin  -lAddressBookLibrary

# Include headers from the AddressBook library (lib dir):
INCLUDEPATH += ../lib
