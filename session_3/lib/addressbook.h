#ifndef ADDRESSBOOK_H
#define ADDRESSBOOK_H

#include "addressbookentry.h"
#include "addressbooklibrary.h"

#include <QList>
#include <QObject>

/**
 * @brief The AddressBook class
 *
 * This class represents our address book itself. It has methods to create new entries, list all
 * existing ones and delete entries again.
 */
class ADDRESSBOOKLIBRARY_EXPORT AddressBook : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Alias
     *
     * This is just an alias definition for convenience
     */
    typedef QList<AddressBookEntry*> Entries;

    /**
     * @brief Constructor
     *
     * The constructor. As we derive from QObject, the constructor should at least
     * take a parent object. The parent becomes the owner of the object created. If the parent
     * is deleted, it will delete all children it owns.
     */
    explicit AddressBook(QObject *parent = 0);

    /**
     * @brief This returns the list of all address book entries we currently store
     */
    Entries entries() const;

    /**
     * @brief Create new entries
     *
     * This will create a new entry and return a pointer to it if possible. If something goes wrong
     * (e.g. out of memory and we cannot allocate another object) this returns a nullptr.
     */
    AddressBookEntry *createEntry();

    /**
     * @brief Delete an entry
     *
     * This will try to delete the given entry. If the entry is owned by the address book,
     * than this will delete the entry and return true. Otherwise, nothing happens and false is
     * returned.
     */
    bool deleteEntry( AddressBookEntry *entry );
    
    /**
     * @brief Clears the address book
     *
     * This removes all entries from the address book.
     */
    void clear();

    /**
     * @brief Assignment operator
     *
     * This operator can be used to "clone" an address book. It deep copies the data from the
     * @p other address book into the current one. If the current address book previously had
     * any entries, they are deleted first.
     */
    AddressBook &operator =( const AddressBook &other );

signals:

    /**
     * @brief A new entry has been added to the address book
     *
     * This signal is emitted wheneber a new entry has been added to the address book.
     */
    void entryAdded( AddressBookEntry *entry );

    /**
     * @brief An entry has been removed from the address book
     *
     * This signal is emitted whenever an entry is deleted from the address book.
     */
    void entryRemoved( AddressBookEntry *entry );

public slots:

private:

    /**
     * @brief The address book entries
     *
     * This is the list of entries in the address book itself.
     */
    Entries m_entries;

};

/*
 * Registers pointers to AddressBook objects in the Meta Object System. This allows us to send
 * variables of this type via Queued Connections (required for inter thread communication).
 */
Q_DECLARE_METATYPE( AddressBook* )

#endif // ADDRESSBOOK_H
