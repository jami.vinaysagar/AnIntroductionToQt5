#include "pluginmanager.h"

#include <QDir>
#include <QPluginLoader>

PluginManager::PluginManager(QObject *parent) : QObject(parent)
{
  
}

void PluginManager::loadPluginsFromDir(const QString directory)
{
  QDir dir( directory );
  for ( auto entry : dir.entryList( QDir::Files ) ) {
    QPluginLoader loader( dir.absoluteFilePath( entry ) );
    if ( loader.load() ) {
      QObject *pluginInstance = loader.instance();
      AddressBookFileFactory *fileFactory = 
          qobject_cast<AddressBookFileFactory*>( pluginInstance );
      if ( fileFactory != nullptr ) {
        m_fileFactories.append( fileFactory );
      }
    }
  }
}

PluginManager::FileFactories PluginManager::fileFactories() const
{
  return m_fileFactories;
}

