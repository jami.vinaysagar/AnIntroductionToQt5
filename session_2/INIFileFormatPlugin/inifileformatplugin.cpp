#include "inifileformatplugin.h"

#include "inifileformat.h"

INIFileFormatPlugin::INIFileFormatPlugin(QObject *parent) : QObject( parent )
{
}

AddressBookFile *INIFileFormatPlugin::createAddressBookFile() const
{
    return new INIFileFormat();
}

QString INIFileFormatPlugin::name() const
{
    return tr( "INI" );
}

QString INIFileFormatPlugin::fileNameExtension() const
{
    return "ini";
}
