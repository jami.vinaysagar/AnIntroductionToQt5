QT       += core gui widgets

CONFIG += c++11

TARGET = AddressBook
TEMPLATE = app
DESTDIR = ../bin

# Do not create an app bundle on Mac OS X:
macx:CONFIG -= app_bundle

INCLUDEPATH += ../lib

LIBS += -L$$DESTDIR -lAddressBookLibrary

SOURCES += main.cpp

HEADERS  += 

FORMS    += 
