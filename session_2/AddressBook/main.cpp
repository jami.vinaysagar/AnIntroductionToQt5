#include "mainwindow.h"
#include "addressbook.h"
#include "addressbookcontroller.h"
#include "pluginmanager.h"

#include <QApplication>
#include <QDir>

int main(int argc, char *argv[])
{
    // Create a QApplication (this is required for any widget based application):
    QApplication a(argc, argv);

    // Create an address book:
    AddressBook ab;
    
    // Create a plugin manager:
    PluginManager pluginManager;

    // Create a QDir pointing to the sub-directory in $appDir/AddressBookPlugins:
    QDir pluginsDir( a.applicationDirPath() );
    if ( pluginsDir.cd( "AddressBookPlugins" ) ) {
        // And load plugins from there:
        pluginManager.loadPluginsFromDir( pluginsDir.absolutePath() );
    }
    
    // Create an address book controller and pass it the address book to use:
    AddressBookController controller( &ab, &pluginManager );

    // Create the main window and pass it the controller to use:
    MainWindow w( &controller );

    // Show the main window:
    w.show();

    // Start the application's event look:
    return a.exec();
}
