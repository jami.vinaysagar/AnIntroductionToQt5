#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include "addressbookfilefactory.h"
#include "addressbooklibrary.h"

#include <QObject>
#include <QList>

/**
 * @brief The PluginManager class
 *
 * This class takes care to load and manage plugins to our AddressBook application.
 */
class ADDRESSBOOKLIBRARY_EXPORT PluginManager : public QObject
{
    Q_OBJECT
public:

    /**
     * @brief Alias type definition
     */
    typedef QList<AddressBookFileFactory*> FileFactories;

    /**
     * @brief Constructor
     */
    explicit PluginManager(QObject *parent = 0);

    /**
     * @brief Loads plugins from the given @p directory
     */
    void loadPluginsFromDir( const QString directory );

    /**
     * @brief Returns the loaded file factories
     *
     * This returns the list of loaded AddressBookFileFactory objects loaded by
     * the plugin manager.
     */
    FileFactories fileFactories() const;

private:

    FileFactories m_fileFactories;
};

#endif // PLUGINMANAGER_H
