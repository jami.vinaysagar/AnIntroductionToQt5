#include "xmlfileformat.h"

// Required in order to parse and create XML files:
#include <QDomDocument>

#include <QFile>

XMLFileFormat::XMLFileFormat(QObject *parent) : QObject(parent)
{

}

bool XMLFileFormat::saveAddressBook(const AddressBook *addressBook, const QString &fileName) const
{
    // Create a new DOM (XML) document:
    QDomDocument doc;

    // Create a new root element:
    QDomElement root = doc.createElement( "addressBook" );

    // Iterate over all address book entries:
    for ( auto entry : addressBook->entries() ) {

        // Create an element for the entry:
        QDomElement entryElement = doc.createElement( "addressBookEntry" );

        // Set the name and birthday as attributes:
        entryElement.setAttribute( "name", entry->name() );
        entryElement.setAttribute( "birthday", entry->birthday().toString() );

        // For the address, create a sub-element with the address as text content:
        QDomElement address = doc.createElement( "address" );
        address.appendChild( doc.createTextNode( entry->address() ) );
        entryElement.appendChild( address );

        // For the phone numbers, create a sub element, which in turn has one element
        // for each phone number:
        QDomElement numbers = doc.createElement( "phoneNumbers" );
        for ( auto number : entry->phoneNumbers() ) {
            QDomElement numberElement = doc.createElement( "phoneNumber" );
            numberElement.appendChild( doc.createTextNode( number ) );
            numbers.appendChild( numberElement );
        }
        entryElement.appendChild( numbers );

        // Append the entry element to the root element:
        root.appendChild( entryElement );
    }

    // Set the root element as document element:
    doc.appendChild( root );

    // Open a file and write the XML (doc.toString() ) to it:
    QFile file( fileName );
    if ( file.open( QIODevice::WriteOnly ) ) {
        file.write( doc.toString().toUtf8() );
        file.close();
        return true;
    }
    return false;
}

bool XMLFileFormat::loadAddressBook(AddressBook *addressBook, const QString &fileName) const
{
    // Open a file for reading:
    QFile file( fileName );
    if ( !file.open( QIODevice::ReadOnly ) ) {
        return false;
    }

    // Create an XML document:
    QDomDocument doc;

    // Load it from the file:
    if ( !doc.setContent( &file ) ) {
        return false;
    }

    // Get the root element:
    QDomElement root = doc.documentElement();

    // Iterate over all 'addressBookEntry' elements:
    QDomElement entryElement = root.firstChildElement( "addressBookEntry" );
    while ( entryElement.isElement() ) {

        // Create a new address book entry:
        auto entry = addressBook->createEntry();
        if ( entry ) {
            // Read the name and birthday attributes:
            entry->setName( entryElement.attribute( "name" )  );
            entry->setBirthday( QDate::fromString( entryElement.attribute( "birthday" ) ) );

            // Get the text from the address child element and set it:
            entry->setAddress( entryElement.firstChildElement( "address" ).text() );

            // Iterate over all phoneNumbers/phoneNumber elements and add them to the list of
            // numbers:
            QStringList phoneNumbers;
            QDomElement phoneNumber = entryElement.firstChildElement( "phoneNumbers" ).firstChildElement( "phoneNumber" );
            while ( phoneNumber.isElement() ) {
                phoneNumbers.append( phoneNumber.text() );
                phoneNumber = phoneNumber.nextSiblingElement( "phoneNumber" );
            }
            entry->setPhoneNumbers( phoneNumbers );
        }
        entryElement = entryElement.nextSiblingElement( "addressBookEntry" );
    }
    return true;
}

